#!/usr/bin/env python3
import threading
import time
from dataclasses import dataclass
from typing import List, Optional
import math

import rospy
from std_msgs.msg import UInt16, Float32

rospy.init_node("main", anonymous=True)

# ros is broken
# https://github.com/ros/ros_comm/issues/1384

def clamp(value, min, max):
    return sorted((min, value, max))[1]

class MotorGroup:
    _publishers: List[rospy.Publisher]
    _raw_output: List[int] = [0, 0, 0, 0]
    speed: int = 0
    diff: int = 0

    def __init__(self):
        # TODO adjust queue_size
        self._publishers = [
            rospy.Publisher("/ima1", UInt16, queue_size=3),
            rospy.Publisher("/ima2", UInt16, queue_size=3),
            rospy.Publisher("/imb1", UInt16, queue_size=3),
            rospy.Publisher("/imb2", UInt16, queue_size=3),
        ]

    def publish(self):
        motor_a = self.speed + self.diff
        motor_b = self.speed - self.diff

        if motor_a > 255:
            overflow = motor_a - 255
            motor_a = motor_a - overflow
            motor_b = motor_b - overflow

        if motor_b > 255:
            overflow = motor_b - 255
            motor_a = motor_a - overflow
            motor_b = motor_b - overflow

        self._raw_output = [0, 0, 0, 0]

        if motor_a > 0:
            self._raw_output[0] = motor_a
        elif motor_a < 0:
            self._raw_output[1] = -motor_a

        if motor_b > 0:
            self._raw_output[2] = motor_b
        elif motor_b < 0:
            self._raw_output[3] = -motor_b

        rospy.logwarn_throttle(1, f"{self.diff=}")
        rospy.loginfo_throttle(1, f"{self._raw_output=}")

        for n, pub in enumerate(self._publishers):
            pub.publish(int(self._raw_output[n]))

    def run(self):
        while True:
            self.publish()
            time.sleep(1e-2)


motor_group = MotorGroup()
motor_group.speed = 230

thread_motor_group = threading.Thread(target=motor_group.run, daemon=True)
thread_motor_group.start()


@dataclass
class CNY:
    _1: Optional[UInt16] = None
    _2: Optional[UInt16] = None
    diff: int = 0

    def _set_1(self, value: UInt16):
        self._1 = value
        self._set_diff()

    def _set_2(self, value: UInt16):
        self._2 = value
        self._set_diff()

    def _set_diff(self):
        if self._1 and self._2:
            self.diff = self._2.data - self._1.data
            rospy.logdebug_throttle(1, f"{self.diff=}")

    def listen(self):
        rospy.Subscriber(name="/cny1", data_class=UInt16, callback=self._set_1)
        rospy.Subscriber(name="/cny2", data_class=UInt16, callback=self._set_2)


cny = CNY()
thread_cny = threading.Thread(target=cny.listen, daemon=True)
thread_cny.start()



class Ultra:
    distance: float = 0

    def __init__(self) -> None:
        rospy.Subscriber(name="/ultra", data_class=Float32, callback=self._set)

    def _set(self, msg: Float32):
        self.distance = msg.data
        rospy.loginfo_throttle(1, f"Ultra: {self.distance}")


ultra = Ultra()

@dataclass
class Controller:
    kp = 0.4
    ki = 0.0
    kd = 0.2
    delta_t = 1e-2

    output = 0
    limit = 200

    def run(self):
        i_term = 0.0
        y_old = 0
        while True:
            y_now = cny.diff

            error = 0 - y_now
            rospy.loginfo_throttle(1, f"{error=}")

            p_term = error * self.kp
            i_term += self.ki * error * self.delta_t
            i_term = clamp(i_term, -50, 50)
            d_term = self.kd * (y_old - y_now) / self.delta_t

            self.output = p_term + i_term + d_term
            self.output = clamp(self.output, -self.limit, self.limit)
            rospy.loginfo_throttle(1, f"{p_term=}")
            rospy.loginfo_throttle(1, f"PID output={self.output}")

            y_old = y_now

            motor_group.diff = self.output

            ## Speed calc
            slowdown = 460 - 23*ultra.distance
            slowdown = clamp(slowdown, 0, 200)
            motor_group.speed = 230-slowdown

            time.sleep(self.delta_t)


controller = Controller()
thread_controller = threading.Thread(target=controller.run, daemon=True)
thread_controller.start()


@dataclass
class ServoController:
    kp = -1

    def __init__(self) -> None:
        self.publisher = rospy.Publisher("/servo", UInt16, queue_size=3)

    def run(self):
        while True:
            y = motor_group.diff
            self.output = self.kp * y
            self.output = clamp(self.output, -100, 100)
            # center point
            self.output += 100
            rospy.loginfo_throttle(1, f"ServoController output: {self.output}")

            self.publisher.publish(int(self.output))


servo_controller = ServoController()
thread_servo_controller = threading.Thread(target=servo_controller.run, daemon=True)
thread_servo_controller.start()





rospy.spin()
