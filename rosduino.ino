#include <HCSR04.h>
#include <ros.h>
#include <std_msgs/Float32.h>
#include <std_msgs/UInt16.h>
#include <stdint.h>
// #include <Servo.h>


#define PIN_IMA1 6
#define PIN_IMA2 8
#define PIN_IMB1 11
#define PIN_IMB2 5

#define PIN_CNY_1 4
#define PIN_CNY_2 2

#define PIN_ULTRA_ECHO 3
#define PIN_ULTRA_TRIG 2

#define PIN_SERVO 4

ros::NodeHandle node_handle;

// Servo myservo;

void motor_callback_a1(const std_msgs::UInt16 &msg) {
  analogWrite(PIN_IMA1, msg.data);
}

void motor_callback_a2(const std_msgs::UInt16 &msg) {
  analogWrite(PIN_IMA2, msg.data);
}

void motor_callback_b1(const std_msgs::UInt16 &msg) {
  analogWrite(PIN_IMB1, msg.data);
}

void motor_callback_b2(const std_msgs::UInt16 &msg) {
  analogWrite(PIN_IMB2, msg.data);
}

// void servo_callback(const std_msgs::UInt16 &msg) {
//   myservo.write(msg.data);
// }

ros::Subscriber<std_msgs::UInt16> subscriber_ima1("ima1", motor_callback_a1);
ros::Subscriber<std_msgs::UInt16> subscriber_ima2("ima2", motor_callback_a2);
ros::Subscriber<std_msgs::UInt16> subscriber_imb1("imb1", motor_callback_b1);
ros::Subscriber<std_msgs::UInt16> subscriber_imb2("imb2", motor_callback_b2);

// ros::Subscriber<std_msgs::UInt16> subscriber_servo("servo", servo_callback);

std_msgs::UInt16 msg_cny1;
std_msgs::UInt16 msg_cny2;

ros::Publisher publisher_cny1("cny1", &msg_cny1);
ros::Publisher publisher_cny2("cny2", &msg_cny2);

UltraSonicDistanceSensor distanceSensor(PIN_ULTRA_TRIG, PIN_ULTRA_ECHO);

std_msgs::Float32 msg_ultra;
ros::Publisher publisher_ultra("ultra", &msg_ultra);

uint16_t counter = 0;

void setup() {
  // myservo.attach(PIN_SERVO);

  node_handle.initNode();

  pinMode(PIN_IMA1, OUTPUT);
  pinMode(PIN_IMA2, OUTPUT);
  pinMode(PIN_IMB1, OUTPUT);
  pinMode(PIN_IMB2, OUTPUT);
  node_handle.subscribe(subscriber_ima1);
  node_handle.subscribe(subscriber_ima2);
  node_handle.subscribe(subscriber_imb1);
  node_handle.subscribe(subscriber_imb2);

  // node_handle.subscribe(subscriber_servo);

  node_handle.advertise(publisher_cny1);
  node_handle.advertise(publisher_cny2);
  node_handle.advertise(publisher_ultra);
}

void loop() {
  // Throttle outgoing traffic
  if (counter == 50) {
    counter = 0;

    msg_cny1.data = analogRead(PIN_CNY_1);
    msg_cny2.data = analogRead(PIN_CNY_2);

    publisher_cny1.publish(&msg_cny1);
    publisher_cny2.publish(&msg_cny2);

    msg_ultra.data = distanceSensor.measureDistanceCm();
    publisher_ultra.publish(&msg_ultra);
  } else {
    counter++;
  }

  node_handle.spinOnce();

  delay(1);
}
