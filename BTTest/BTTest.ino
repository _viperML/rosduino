#include <SoftwareSerial.h>

SoftwareSerial BT(12, 13);

void setup() {
    Serial.begin(9600);
    BT.begin(9600);
}

void loop() {
    Serial.println("Hello-Wire");
    BT.println("Hello-BT");

    delay(100);
}
