#!/usr/bin/env python3
import rospy
from std_msgs.msg import UInt16

rospy.init_node("main", anonymous=True)

publishers = [
    rospy.Publisher("/ima1", UInt16, queue_size=3),
    rospy.Publisher("/ima2", UInt16, queue_size=3),
    rospy.Publisher("/imb1", UInt16, queue_size=3),
    rospy.Publisher("/imb2", UInt16, queue_size=3),
]

while not rospy.is_shutdown():
    for pub in publishers:
        pub.publish(0)
