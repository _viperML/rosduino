# Line Follower Robot

This repository holds the source code for a LFR that was developed during my Master's degree.
## Demo

![Demo](https://gitlab.com/_viperML/rosduino/-/raw/assets/video.mp4)

## High level overview

The roboto uses an Arduino board which has connections to the motors and sensors. The sensors and motors are read and written in an infinite loop.

The Arduino program also uses rosserial, a library that lets it connect to a PC host. This host also runs the "server-side" component of rosserial. In the end, this allows the Arduino board to connect to a ROS network on the host PC. It was discussed to use a Bluetooth connection instead of a cable, but it was not possible to be done.

The arduino program is very simple, as all it does is to read and write from the ROS topics into the sensors and actuators.

- Sensors: read and published without modifications. For example, publishes the CNY sensor data directly on `/cny1` and `/cny2`
- Motors: directly applies any value published in the motors' topics (`/ima1`, `/ima2`, `/imb1` and `/imb2`).

`main.py` is a python script that runs on thr "host" PC. It connects to a `roscore` instance running on the same PC. This programs calculates all the logic of the robot, by reading and writing to the topics of ROS. On a high level, it runs multiple concurrent threads that store the ROS readings in objects, and then read/write to this objects in parallel. For example, a thread reads the `CNY` in a endless loop, another different thread uses these readings to calculate the controller, etc.

The controller is based on a basic `PID` design, which uses the difference of CNY's as the error signal. However, a simple proportional controller proved to be enough for this robot.

Finally, an ultrasonic sensor is used to measure the distance of the robot to an object. This distance is used to stop the robot before hitting hit.

![Diagram](https://gitlab.com/_viperML/rosduino/-/raw/assets/diagram.png)



## Installation

The software was developed against ROS 1, for the distribution Noetic. The most "painless" way of using ROS Noetic is by using the officially supported OS (Ubuntu 20.04) and following the [official installation procedure](https://wiki.ros.org/noetic/Installation/Ubuntu). This would provide a ROS installation, containing:
  - `roscore`
  - `roslaunch`
  - `rospy` (python library)

To develop with arduino, `arduino-cli` was used. It is a single binary which can be used to develop Arduino programs. It can be downloaded from the "releases" page in the [arduino-cli repository](https://github.com/arduino/arduino-cli).

The `HCSR04` library was used for the ultrasonic sensor, which can be installed with: `arduino-cli lib install HCSR04`.

Finally, `rosserial_arduino` must be installed according to the [official documentation](https://wiki.ros.org/rosserial_arduino/Tutorials/Arduino%20IDE%20Setup).

## Usage

The procedure used to connect to the robot is:

- Connect the robot via USB to the host PC
  - If the board is fresh, upload the code with `make upload`
- Run `roscore`
- Run rosserial with `make node`
- Run `main.py`
