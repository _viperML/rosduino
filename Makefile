# -*- mode: makefile -*-

PORT := /dev/ttyUSB0

ARDUINO = arduino-cli compile -b arduino:avr:uno --port ${PORT}

.PHONY: db build upload

build:
	@${ARDUINO}

db:
	@rm -vfr builddir
	@${ARDUINO} --build-path builddir --output-dir builddir --only-compilation-database

upload:
	@${ARDUINO} --upload

node:
	roslaunch --wait ./node.launch

venv:
	@rm -rvf .venv
	@python -m venv --system-site-packages .venv
